<?php

/**
 * Implements hook_preprocess_TEMPLATE().
 */
function volta_preprocess_block(&$variables) {
  $block = $variables['block'];
  if ($block->region == 'navigation' && _volta_is_menu_block($block)) {
    $variables['attributes_array']['class'][] = 'block--nav-bar';
  }
}
