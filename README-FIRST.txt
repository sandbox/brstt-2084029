Volta started as a clone and is now a fork of Ohm. I'll stop at this initial juncture https://drupal.org/node/2084815#comment-7845273 and continue along in a new theme that is an omega-extended subtheme. 

Volta is intended to be used for hands-on educational and discovery purposes. You don't have to subtheme it, just use it as-is and tinker and play around with the code to see how Ohm differs from Omega extended. Please refer to the latest releases of Ohm for its particular standards and practices that continue to develop after this snapshot was created. 

I borrowed from Zen's config.rb for its simplicity and readability. No need to do anything with this file; you can leave it in development mode. 

____________________________________________________________

Prerequisites: Digging into Sass and Ruby with a dash of RVM.

You could play around with the css and templates in this theme and completely ignore the Sass, though you wouldn't learn very much.  

You could also install prerequisites individually from each project or corresponding repository. Note that Omega offers a process that's smart, easy and manageable.

Easy? Sure. It's as easy as getting RVM. Making sure you can run it as a function from the prompt. Then run 'rvm requirements'. Then run 'bundle install'. 

See fubhy's post for background information about getting set up with Sass, Ruby and RVM. 
https://drupal.org/node/1936970

See my follow-up post about getting set up on Ubuntu.  
https://drupal.org/node/2052955

Here's a 3 min screencast that covers installing everything required via rvm, including ruby and bundler, on Ubuntu : http://www.youtube.com/watch?v=RDuSrP3w06Y

With the stack installed,  you're good to go and can edit around with scss files and  from the '/themes/volta$' prompt run 'compass compile' to generate the corresponding css files. You can also look into a few auto-pilot options. Look into 'compass watch' and/or guard with 'drush ogrd volta'

Also handy: 
FireSass for Firebug on Firefox  https://addons.mozilla.org/en-US/firefox/addon/firesass-for-firebug/
 Set to work with the development environment that's set by default in config.rb. Install the extension, compile the sass and firebug will show the corresponding line numbers in your .scss files. 

Chrome devtools See http://remysharp.com/2013/07/18/my-workflow-v3-full-coding-stack/ for an excellent how-to enable and demo video. Skip ahead to 1m58s to see how you will add your theme as a workspace so you can edit and save Sass and other files from within the browser. Watch from the beginning to see how to get started.  http://youtu.be/X-X9w4Pso5w?t=1m58s

